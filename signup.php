<?php
include('db.php');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if ($_POST['confirm'] === $_POST['password']) {
        // pripremamo upit
        $sql = "INSERT INTO users (email, password) VALUES ()";

        $statement = $connection->prepare($sql);

        // izvrsavamo upit
        $statement->execute();
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="favicon.ico">
    <title>Vivify oglasi - signup</title>

    <link rel="stylesheet" href="css/styles.css">
</head>
<body class="va-l-page va-l-page--single">
    <?php include('header.php') ?>

    <div class="va-l-container">
        <main class="va-l-page-content">
            <div class="profile">
                <header class="va-l-page-header">
                    <h1>Create user</h1>
                </header>

                <form action="/signup.php" method="post">
                    <div class="va-c-form va-c-new-post">
                        <div class="va-c-form-group">
                            <label class="va-c-control-label">Email</label>
                            <input name="email" type="text" class="va-c-form-control">
                        </div>

                        <div class="va-c-form-group">
                            <label class="va-c-control-label">Password</label>
                            <input name="password" type="password" class="va-c-form-control">
                        </div>

                        <div class="va-c-form-group">
                            <label class="va-c-control-label">Confirm Password</label>
                            <input name="confirm" type="password" class="va-c-form-control">
                        </div>

                        <div class="va-c-form-group">
                            <button class="va-c-btn va-c-btn--primary">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </main>
    </div>

    <?php include('footer.php'); ?>
</body>
</html>

